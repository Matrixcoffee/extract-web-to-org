#!/bin/bash

PYTHONFLAGS=

failed ()
{
	echo "Failed" "$@"
	exit 1
}

try ()
{
	echo "> $*"
	"$@" || failed to execute \""$@"\"
}

while [ "$1" ]; do case "$1" in
	-i)	PYTHONFLAGS="$PYTHONFLAGS $1"
		shift
		;;
	--)	shift
		break
		;;
	*)	break
		;;
esac; done

ERRORS=()

mkdir -p untracked

EXTRACTORS=("$@")
[ ${#EXTRACTORS} = 0 ] && EXTRACTORS=(*.py)

for F in "${EXTRACTORS[@]}"; do
	BN="${F%.py}"
	echo "################# $BN #################"
	#try python3 $PYTHONFLAGS "$F" > "untracked/$BN".org 2> "untracked/$BN".err
	try python3 $PYTHONFLAGS "$F"
done
