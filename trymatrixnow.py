import re
import sys


def eprint(*args, **kwargs):
	kwargs['file'] = sys.stderr
	print(*args, **kwargs)


class PrintWrapper:
	def __init__(self, filehandle):
		self.filehandle = filehandle

	def print(self, *args, **kwargs):
		kwargs['file'] = self.filehandle
		return print(*args, **kwargs)


class MatrixProject:
	RE_URL = re.compile("\\((http[^)]+)\\)")
	RE_NAME = re.compile("\\([^)]*\\)")

	def __init__(self):
		self.headers = {}
		self.freeform = []
		self.freeform_pos = 0

	def add_header(self, k, v):
		self.headers[k] = v
		fun = getattr(self, "hdr_" + k, None)
		if fun is not None: fun(v)

	def hdr_categories(self, v):
		cats = set(map(lambda x:x.lower(), v.split()))
		cats.discard('projects')
		self.headers['categories'] = tuple(cats)
		#eprint("Categories set:", repr(cats))

	def get_category(self):
		try:
			return self.headers['categories'][0].lower()
		except (KeyError, TypeError, IndexError):
			return None

	def get_urls(self):
		urls = []
		for line in self.freeform:
			pos = 0
			while True:
				m = self.RE_URL.search(line[pos:])
				if not m: break
				urls.append(m.group(1))
				pos += m.end()
		return urls

	def get_url(self):
		if 'home' in self: return self['home']
		if 'repo' in self: return self['repo']
		urls = self.get_urls()
		if not urls: return None
		return urls[0]

	def get_description(self):
		desc = self['title'] + " is"
		try:
			maturity = self['maturity'].lower()
			if maturity[0] in "aeuoi": maturity = "an " + maturity
			else: maturity = "a " + maturity
			desc = desc + " " + maturity
		except KeyError:
			pass
		cat = self.get_category()
		if cat:
			if cat == 'other': cat = 'project'
			if cat == 'as': cat = 'AS (application service)'
			desc = desc + " " + cat
		written = False
		if 'language' in self:
			desc = desc + " written in " + self['language']
			written = True
		if 'author' in self:
			if written:	desc = desc + " by " + self['author']
			else:		desc = desc + " written by " + self['author']
		if 'description' in self: desc = desc + ". " + self['description']

		if not desc.endswith("."): desc += "."

		return desc

	def get_name(self):
		name = self['title']
		while True:
			m = self.RE_NAME.search(name)
			if not m: return name
			name = name[:m.start()] + name[m.end():]
			name = " ".join(name.split())

	def add_freeform(self, line):
		if line == "":
			if self.freeform_pos < len(self.freeform):
				self.freeform_pos += 1
			return

		try:
			self.freeform[self.freeform_pos] = self.freeform[self.freeform_pos] + " " + line
		except IndexError:
			self.freeform.append(line)

	def __getitem__(self, k):
		return self.headers[k]

	def __contains__(self, k):
		return k in self.headers

	def parse_from_file(self, filename):
		self.filename = filename
		with open(filename, "r") as fh:
			return self.parse_from_filehandle(fh)

	def parse_from_filehandle(self, fh):
		trilines = 0
		while True:
			line = fh.readline()
			if line == "": break
			line = line.strip()
			#eprint("Line: line")
			if line == '---':
				trilines += 1
				continue
			if trilines == 1:
				kv = line.split(": ", 1)
				#eprint(repr(kv))
				if len(kv) == 2:
					k, v = kv
					k = k.lower()
					#eprint("k:", k)
					#eprint("v:", v)
					self.add_header(k, v)
			if trilines > 1:
				if line.startswith('#'): continue
				if line.startswith('!['): continue
				self.add_freeform(line)

	def write_as_org(self, fh):
		out = PrintWrapper(fh)

		#eprint("Category:", repr(self.get_category()))
		#eprint("Freeform:", repr(self.freeform))
		#eprint("URLs:", repr(self.get_urls()))

		if not 'title' in self: return False

		if len(self.freeform) > 0 and 'meta http-equiv="refresh"' in self.freeform[0]:
			return False

		title = self['title']
		name = self.get_name()

		out.print("*", title)

		if 'author' in self:
			out.print("** Who is the author of %s?" % name)
			out.print()
			out.print("The author of %s is %s." % (title, self['author']))
			out.print()

		wherestatements = []
		if 'home' in self and 'repo' in self:
			wherestatements.append("{}'s homepage is at [[{}]] and its source can be found at [[{}]]".format(title, self['home'], self['repo']))
		else:
			url = self.get_url()
			if url: 
				wherestatements.append("{} can be found at [[{}]]".format(title, url))

		if 'home' in self:
			out.print("** Where is the {} homepage?".format(name))
			out.print()
			out.print("{} has a homepage at [[{}]].".format(title, self['home']))
			out.print()

		if 'repo' in self:
			out.print("** Where is the {} source code?".format(name))
			out.print()
			out.print("{} has a source repository at [[{}]].".format(title, self['repo']))
			out.print()

		if 'room' in self:
			wherestatements.append("{} has a room at {}".format(title, self['room'].strip('"')))
			out.print("** Where is the {} room?".format(name))
			out.print()
			out.print("{} has a room at {}.".format(title, self['room'].strip('"')))
			out.print()

		if wherestatements:
			out.print("** Where can I find %s?" % name)
			out.print()
			out.print(". ".join(wherestatements) + ".")
			out.print()

		if 'language' in self:
			out.print("** What is {} written in?".format(name))
			out.print()
			out.print("{} is written in {}.".format(title, self['language'].strip('"')))
			out.print()

		if 'maturity' in self:
			out.print("** How mature is %s?" % name)
			out.print()
			out.print("%s is %s." % (title, self['maturity'].lower()))
			out.print()

		if len(self.freeform) > 0:
			out.print("** What is %s?" % name)
			out.print()
			out.print(self.get_description())
			out.print()
			out.print(self.freeform[0])
			out.print()

		return True


if __name__ == '__main__':
	import os
	import os.path
	import time

	maindir = "untracked"
	projectname = "trymatrixnow"
	projectdir = os.path.join(maindir, projectname)
	artifact = os.path.join(maindir, projectname + ".org")
	matrixorggit = os.path.join(projectdir, "matrix.org")
	jekyllfiles = os.path.join(matrixorggit, "jekyll", "_posts", "projects")
	repo = "https://github.com/matrix-org/matrix.org.git"

	if not os.path.isdir(maindir):
		raise Exception('Directory "{}" not found. Is the project configured?'.format(maindir))
	if not os.path.isdir(projectdir):
		os.mkdir(projectdir)
	if not os.path.isdir(matrixorggit):
		os.system("cd {} && git clone {}".format(projectdir, repo))
	if not os.path.isdir(matrixorggit):
		raise Exception('Cloning of "{}" failed.'.format(repo))

	age = time.time() - os.stat(matrixorggit).st_mtime
	if age > 64800: # 18 hours
		os.system("cd {} && git pull".format(matrixorggit))

	files = os.listdir(jekyllfiles)
	nreads = 0
	nwrites = 0

	with open(artifact, "w") as orgfile:
		for fn in sorted(files):
			if not fn.startswith("2") or not fn.endswith(".md"): continue
			eprint("File:", repr(fn))
			mp = MatrixProject()
			mp.parse_from_file(os.path.join(jekyllfiles, fn))
			nreads += 1
			if mp.write_as_org(orgfile):
				nwrites += 1

	eprint("{}: {} files read, {} items written to 1 file".format(projectname, nreads, nwrites))
