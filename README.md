# Web to org-mode extractors
[Web to org-mode extractors](https://gitlab.com/Matrixcoffee/extract-web-to-org)
is a collection of data extractors outputting in [org-mode](https://orgmode.org/)
format, with the specific purpose of making on-line resources available to FAQBot.

## Why does it exist?
Users benefit from [FAQBot](https://gitlab.com/Matrixcoffee/FAQBot) having access to more information,
such as the [Try Matrix Now!](https://matrix.org/docs/projects/try-matrix-now.html) online directory of
Matrix projects. [Matrix Knowledge Base](https://gitlab.com/Matrixcoffee/matrix-knowledge-base) and its
maintainers also benefit because the information doesn't need to be duplicated and kept in sync.

These extractors started life deep inside the underbelly of
[FAQBot](https://gitlab.com/Matrixcoffee/FAQBot), but were spun out as they
don't really depend on FAQBot, and having them separate helps to keep FAQBot's
code base clean.

## What does it do?
It downloads things and converts them into [org-mode](https://orgmode.org/)
format, ready to be used by [FAQBot](https://gitlab.com/Matrixcoffee/FAQBot).

## Status
**Alpha**. Operational, but under construction.

## Recommended Installation
Clone the repository somewhere and add run the script:
```
$ /bin/bash run-extractors.bash
```
That's it! No configuration necessary, as everything is already pre-configured (and hardcoded).

Note that the script expects to have unrestricted\* access to the internet. It
is, after all, a collection of extractors for on-line resources.

\* It should work on [Tails](https://tails.boum.org), though.

## Dependencies
* Python >= 3.2
* (soon) [transformat](https://gitlab.com/Matrixcoffee/transformat)

## License
Copyright 2018 [@Coffee:matrix.org](https://matrix.to/#/@Coffee:matrix.org)

   > Licensed under the Apache License, Version 2.0 (the "License");
   > you may not use this file except in compliance with the License.

   > The full text of the License can be obtained from the file called [LICENSE](LICENSE).

   > Unless required by applicable law or agreed to in writing, software
   > distributed under the License is distributed on an "AS IS" BASIS,
   > WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   > See the License for the specific language governing permissions and
   > limitations under the License.
